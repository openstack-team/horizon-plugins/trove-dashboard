Source: trove-dashboard
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 openstack-dashboard (>= 3:23.3.0),
 python3-coverage,
 python3-ddt,
 python3-django,
 python3-hacking,
 python3-openstackdocstheme,
 python3-oslo.log,
 python3-selenium,
 python3-swiftclient,
 python3-testscenarios,
 python3-testtools,
 python3-troveclient,
 python3-xvfbwrapper,
 subunit,
 testrepository,
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/openstack-team/horizon-plugins/trove-dashboard.git
Vcs-Browser: https://salsa.debian.org/openstack-team/horizon-plugins/trove-dashboard
Homepage: https://opendev.org/openstack/trove-dashboard

Package: python3-trove-dashboard
Architecture: all
Depends:
 openstack-dashboard (>= 3:23.3.0),
 python3-django,
 python3-oslo.log,
 python3-pbr,
 python3-swiftclient,
 python3-troveclient,
 ${misc:Depends},
 ${python3:Depends},
Breaks:
 python3-django-horizon (<< 3:19.2.0-2~),
Description: Database as a Service for OpenStack - dashboard plugin
 Trove is Database as a Service for Openstack. It's designed to run entirely on
 OpenStack, with the goal of allowing users to quickly and easily utilize the
 features of a relational database without the burden of handling complex
 administrative tasks. Cloud users and database administrators can provision
 and manage multiple database instances as needed. Initially, the service will
 focus on providing resource isolation at high performance while automating
 complex administrative tasks including deployment, configuration, patching,
 backups, restores, and monitoring. Trove is designed to support a
 single-tenant database within a Nova instance. There is no restrictions on how
 Nova is configured, since Trove interacts with other OpenStack components
 purely through the API.
 .
 This package provides the Trove dashboard plugin.
